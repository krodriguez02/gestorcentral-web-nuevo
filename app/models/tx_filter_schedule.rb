class TxFilterSchedule < ApplicationRecord
  has_many :tx_schedule_messages, :foreign_key => "schedule_id", dependent: :destroy
end
