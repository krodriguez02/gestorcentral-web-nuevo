class GroupsController < ApplicationController
  before_action :set_group, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  respond_to :html, :js

  # @@group = "Group"

  # GET /groups
  # GET /groups.json
  def index
    @id_usuario = current_user.id
    @id_aplicacion = 25
    @idTipoReg = 11 #Index
    @fecha = Time.now.strftime("%F")
    @hora = Time.now.strftime("%k:%M:%S.00000")
    @men = "Groups Index"


    @tbit = Tbitacora.new
    @tbit.IdUsuario = @id_usuario
    @tbit.IdAplicacion = @id_aplicacion
    @tbit.IdTipoReg = @idTipoReg #Index
    @tbit.Fecha = @fecha
    @tbit.Hora = @hora
    @tbit.Mensaje = @men
    @tbit.save

    # if current_user
    #   @cu = current_user.profile_id
    #   if @cu == 1
    #     @per = Permission.where("view_name = 'Group' and profile_id = ?", @cu)
    #     @per.each do |permisos|
    #       @uno = permisos.view_name
    #       @crearGroup = permisos.crear
    #       @editarGroup = permisos.editar
    #       @leerGroup = permisos.leer
    #       @eliminarGroup = permisos.eliminar
    #
    #       if permisos.view_name == @@group
    #
    #         @@crear = permisos.crear
    #         @@editar = permisos.editar
    #         @@leer = permisos.leer
    #         @@eliminar = permisos.eliminar
    #
    #         @crear = permisos.crear
    #         @editar = permisos.editar
    #         @leer = permisos.leer
    #         @eliminar = permisos.eliminar
    #       end
    #     end
    #     if ((@@leer == 2) || (@@crear == 8) || (@@editar == 4) || (@@eliminar == 1))
    @groups = Group.all
    @consulta = Referenciadealerta.select(:campo).group(:campo)



    #     else
    #       @Without_Permission = 100
    #       redirect_to home_index_path, :alert => t('all.not_access')
    #     end
    #   else
    #     @Without_Permission = 100
    #     redirect_to home_index_path, :alert => t('all.not_access')
    #   end
    # else
    #   @Without_Permission = 100
    #   redirect_to new_user_session_path, :alert => t('all.please_continue')
    # end
  end

  # GET /groups/1
  # GET /groups/1.json
  def show
    @id_usuario = current_user.id
    @id_aplicacion = 25
    @idTipoReg = 13 #Show
    @fecha = Time.now.strftime("%F")
    @hora = Time.now.strftime("%k:%M:%S.00000")
    @men = "Email Groups Show"

    @tbit = Tbitacora.new
    @tbit.IdUsuario = @id_usuario
    @tbit.IdAplicacion = @id_aplicacion
    @tbit.IdTipoReg = @idTipoReg #Index
    @tbit.Fecha = @fecha
    @tbit.Hora = @hora
    @tbit.Mensaje = @men
    @tbit.save
  end

  # GET /groups/new
  def new
    # if current_user
    #   @cu = current_user.profile_id
    #   if @cu == 1
    #     @per = Permission.where("view_name = 'Group' and profile_id = ?", @cu)
    #     @per.each do |permisos|
    #       @uno = permisos.view_name
    #       @crearGroup = permisos.crear
    #       if permisos.view_name == @@group
    #         @@crear = permisos.crear
    #         @crear = permisos.crear
    #       end
    #     end
    #     if ((@@crear == 8))
    @group = Group.new
    @btn = false
    @consulta = Referenciadealerta.select(:campo).group(:campo)
    #     else
    #       @Without_Permission = 100
    #       redirect_to home_index_path, :alert => t('all.not_access')
    #     end
    #   else
    #     @Without_Permission = 100
    #     redirect_to home_index_path, :alert => t('all.not_access')
    #   end
    # else
    #   @Without_Permission = 100
    #   redirect_to new_user_session_path, :alert => t('all.please_continue')
    # end
  end

  # GET /groups/1/edit
  def edit

    # if current_user
    #   @cu = current_user.profile_id
    #
    #   if @cu == 1
    #     @per = Permission.where("view_name = 'Group' and profile_id = ?", @cu)
    #     @per.each do |permisos|
    #       @uno = permisos.view_name
    #       @@editarGroup = permisos.editar
    #       if permisos.view_name == @@group
    #         @@editar = permisos.editar
    #         @editar = permisos.editar
    #       end
    #     end
    #     if ((@@editar == 4))
    @btn = true
    @consulta = Referenciadealerta.select(:campo).group(:campo)
    #     else
    #       @Without_Permission = 100
    #       redirect_to home_index_path, :alert => t('all.not_access')
    #     end
    #   else
    #     @Without_Permission = 100
    #     redirect_to home_index_path, :alert => t('all.not_access')
    #   end
    #
    # else
    #   @Without_Permission = 100
    #   redirect_to new_user_session_path, :alert => t('all.please_continue')
    # end
  end

  # POST /groups
  # POST /groups.json
  def create
    @id_usuario = current_user.id
    @id_aplicacion = 25
    @idTipoReg = 9 #Create
    @fecha = Time.now.strftime("%F")
    @hora = Time.now.strftime("%k:%M:%S.00000")
    @men = "Email Groups New"
    # alertas = cuando se entre a editar alerta se va a guardar el id de tbitacora(alerta que se está atendiendo) y el id de producto, transaccional o perfiles <== aun no


    @tbit = Tbitacora.new
    @tbit.IdUsuario = @id_usuario
    @tbit.IdAplicacion = @id_aplicacion
    @tbit.IdTipoReg = @idTipoReg
    @tbit.Fecha = @fecha
    @tbit.Hora = @hora
    @tbit.Mensaje = @men
    @tbit.save
    @group = Group.new(group_params)

    @group.detail = params[:campo1]
    @group.reference = params[:campo5]
    @group.area = params[:campo3]
    @group.hist_alert = params[:campo4]
    @group.hist_trans = params[:campo5]
    @group.fraude = params[:camporadio]

    respond_to do |format|

      if Group.where("name like ?", @group.name).exists?
        format.html {
          @error = t('notice.blackup')
          render action: :new
        }
      else
        if params[:save_group]
          if @group.save
            format.html {redirect_to groups_path, notice: t('notice.succes')}
            format.json {render :show, status: :created, location: @group}
          else
            format.html {render :new}
            format.json {render json: @group.errors, status: :unprocessable_entity}
          end
        elsif params[:save_mails]
          if @group.save
            format.html {redirect_to :controller => 'mail_groups', :action => 'new', :id => @group.id, notice: t('notice.created', resource: t('notice.group'))}
            format.json {render :show, status: :created, location: @group}
          else
            format.html {render :new}
            format.json {render json: @group.errors, status: :unprocessable_entity}
          end
        end
      end
    end
  end

  # PATCH/PUT /groups/1
  # PATCH/PUT /groups/1.json
  def update
    @id_usuario = current_user.id
    @id_aplicacion = 25
    @idTipoReg = 10 #Edit
    @fecha = Time.now.strftime("%F")
    @hora = Time.now.strftime("%k:%M:%S.00000")
    @men = "Email Groups Edit"

    @tbit = Tbitacora.new
    @tbit.IdUsuario = @id_usuario
    @tbit.IdAplicacion = @id_aplicacion
    @tbit.IdTipoReg = @idTipoReg #Edit
    @tbit.Fecha = @fecha
    @tbit.Hora = @hora
    @tbit.Mensaje = @men
    @tbit.save
    @per = Profile.where("id = ?", params[:id])

    @group.detail = params[:campo1]
    @group.reference = params[:campo5]
    @group.area = params[:campo3]
    @group.hist_alert = params[:campo4]
    @group.hist_trans = params[:campo5]
    @group.fraude = params[:camporadio]

    respond_to do |format|
      if params[:save_group]
        if @group.update(group_params)
          format.html {redirect_to groups_path, notice: t('notice.update')}
          format.json {render :show, status: :ok, location: @group}
        else
          format.html {render :edit}
          format.json {render json: @group.errors, status: :unprocessable_entity}
        end
      elsif params[:save_mails]
        if @group.update(group_params)
          format.html {redirect_to :controller => 'mail_groups', :action => 'new', :id => @group.id, notice: t('notice.created', resource: t('notice.group'))}
          format.json {render :show, status: :created, location: @group}
        else
          format.html {render :new}
          format.json {render json: @group.errors, status: :unprocessable_entity}
        end
      end
    end
  end


  def destroy
    # if current_user
    #   @cu = current_user.profile_id
    #   if @cu == 1
    #     @per = Permission.where("view_name = 'Group' and profile_id = ?", @cu)
    #     @per.each do |permisos|
    #       @uno = permisos.view_name
    #       @@eliminarGroup = permisos.eliminar
    #       if permisos.view_name == @@group
    #         @@eliminar = permisos.eliminar
    #         @eliminar = permisos.eliminar
    #       end
    #     end
    #     if ((@@eliminar == 1))
    @id_usuario = current_user.id
    @id_aplicacion = 25
    @idTipoReg = 12 #Index
    @fecha = Time.now.strftime("%F")
    @hora = Time.now.strftime("%k:%M:%S.00000")


    @tbit = Tbitacora.new
    @tbit.IdUsuario = @id_usuario
    @tbit.IdAplicacion = @id_aplicacion
    @tbit.IdTipoReg = @idTipoReg #Index
    @tbit.Fecha = @fecha
    @tbit.Hora = @hora

    @mails = MailGroup.where("group_id = ?", @group.id);
    @mails.each do |mail|
      mail.destroy
    end

    @tbit.Mensaje = @group.to_json
    @tbit.save

    @group.destroy
    respond_to do |format|
      format.js
      format.html {redirect_to post_url, notice: t('notice.destroy')}
      format.json {head :no_content}
    end
    #     else
    #       @Without_Permission = 100
    #       redirect_to home_index_path, :alert => t('all.not_access')
    #     end
    #
    #   else
    #     @Without_Permission = 100
    #     redirect_to home_index_path, :alert => t('all.not_access')
    #   end
    #
    # else
    #   @Without_Permission = 100
    #   redirect_to new_user_session_path, :alert => t('all.please_continue')
    # end

  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_group
    @group = Group.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def group_params
    params.require(:group).permit(:name, :mailGroups)
  end
end
