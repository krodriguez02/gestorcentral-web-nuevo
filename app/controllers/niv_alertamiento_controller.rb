class NivAlertamientoController < ApplicationController
  @@byLevel = "AlertingAndProductivityLevels"

  def index
    if current_user

      @id_usuario = current_user.id
      @id_aplicacion = 25
      @idTipoReg = 11 #Index
      @fecha = Time.now.strftime("%F")
      @hora = Time.now.strftime("%k:%M:%S.00000")
      @men = "Niveles de Alertamiento y Productividad"


      @tbit = Tbitacora.new
      @tbit.IdUsuario = @id_usuario
      @tbit.IdAplicacion = @id_aplicacion
      @tbit.IdTipoReg = @idTipoReg #Index
      @tbit.Fecha = @fecha
      @tbit.Hora  = @hora
      @tbit.Mensaje = @men
      @tbit.save

      @cu = current_user.profile_id

      if @cu != 0
        @per = Permission.where("view_name = 'AlertingAndProductivityLevels' and profile_id = ?", @cu)

        @per.each do |permisos|
          @uno = permisos.view_name
          @crearXls = permisos.crear
          @leerLevel = permisos.leer

          if permisos.view_name == @@byLevel

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end
        end

        if current_user.habilitado == 0

          if (@@leer == 2)
            # ---------------------------------------------------- Alertas por estado
            @alertas = Tbitacora.where(:IdTipoReg => 3)
            @alertas_count = Tbitacora.where(:IdTipoReg => 3).count()

            @tipos_alertas = Testadosxalerta.all

            arreglo_count = Array.new
            @tipos_alertas.each do |es|
              @alertas_count = Tbitacora.where(:IdTipoReg => 3).where(:IdEstado => es.id).count()

              valor = {tipo: '', cantidad: ''}
              valor[:tipo] = es.Descripcion
              valor[:cantidad] = @alertas_count

              arreglo_count.push(valor)

            end

            @arreglo_count_alertas_estados = arreglo_count

            # ------------------------------------------------- Alertas por Usuario
            @alertas_atn = Tbitacora.where(:IdTipoReg => 3).where.not(:IdUsuario => 'N/A')
            @alertas_atn_count = Tbitacora.where(:IdTipoReg => 3).where.not(:IdUsuario => 'N/A').count()

            @users = User.all

            arreglo_usu = Array.new
            @users.each do |us|
              @alertas_count_us = Tbitacora.where(:IdTipoReg => 3).where(:IdUsuario => us.id).count()

              valor = {usuario: '', usr_name: '', cantidad: ''}
              valor[:usuario] = us.name + ' ' + us.last_name
              valor[:usr_name] = us.user_name
              valor[:cantidad] = @alertas_count_us

              arreglo_usu.push(valor)
            end

            @arreglo_usu = arreglo_usu


            @campos = Tcamposlay.all
            @estados = Testadosxalerta.all
            @usuarios = User.all
            @grupos = Tgrupos.all
            @filtros = TxFilters.all

            per = Tperfiles.all
            gon.perfiles = per
            gon.false = true

            #BUSQUEDAS
            if params[:search_for_states].present?
              @search_for_states = params[:search_for_states]
              puts 'busco para estados'
              @par_estados = params[:valor_estados]
              @estados_busqueda = @par_estados.split(',')
              @usuarios_states = params[:users_states]
              @producto = params[:product_states]
              @filtro = params[:filter_states]
              @grupo = params[:groups_states]
              @perfil = params[:profile_states]
              @fecha_inicio = params[:inicio_states]
              @fecha_fin = params[:fin_states]
              @campos_lay = params[:field_layout_states]
              @valor_lay = params[:value_lay_states]
              @fraude = params[:fraud_states]
              query = ""


              @count_states = 0
              if @estados_busqueda.length == 1
                query = "IdEstado = " + @estados_busqueda[0]
              else
                @estados_busqueda.each do |st|
                  @count_states += 1

                  if @count_states == 1
                    query = "(IdEstado = " + st
                  else
                    if @count_states < @estados_busqueda.length
                      query = query + " or IdEstado = " + st
                    else
                      query = query + " or IdEstado = " + st + ")"
                    end
                  end
                end
              end


              # Se ingresó otro parámetro de búsqueda además del estado
              if @usuarios_states != "" || @producto != "" || @filtro != "" || @grupo != "" || !@perfil.nil? || @fecha_inicio != "" || @fecha_fin != "" || @campos_lay != "" || !@valor_lay.nil? || !@fraude.nil?
                if @usuarios_states != ""
                  query = query + " and IdUsuario = '" + @usuarios_states.to_s + "'"
                end

                if @producto != ""
                  if @producto == "1"
                    query = query + " and IdProducto = 1"

                    if @filtro != ""
                      query = query + " and IdFiltro = " + @filtro
                    end
                  elsif @producto == "2"
                    query = query +" and IdProducto = 2"

                    if @grupo != ""
                      query = query + " and IdGrupo = " + @grupo

                      if !@perfil.nil? && @perfil != ""
                        query = query + " and IdPerfil = " + @perfil
                      end
                    end
                  end
                end

                if @fecha_inicio != ""
                  @fecha_inicio = @fecha_inicio.to_date.strftime('%Y-%m-%d')
                  if @fecha_fin != ""
                    @fecha_fin = @fecha_fin.to_date.strftime('%Y-%m-%d')
                    query = query + " and Fecha between '" + @fecha_inicio + "' and '" + @fecha_fin + "' and Hora between '00:00:00.00000' and '23:59:59.99999'"
                  end
                end

                if @campos_lay != ""
                  if @valor_lay != "" && !@valor_lay.nil?
                    @campo = Tcamposlay.where(:IdCampo => @campos_lay).first
                    query = query + " and Referencia like '%" + @campo.Nombre + " = " + @valor_lay.to_s + "%'"
                  end
                end

                if !@fraude.nil?
                  if @fraude == "1"
                    query = query + " and FalsoPositivo = 1"
                  elsif @fraude == "0"
                    query = query + " and FalsoPositivo = 0"
                  end
                end
              end

              # @resultado_alertas = Tbitacora.where(:IdTipoReg => 3).page(params[:page]).per(10)
              @resultado_alertas = Tbitacora.where(query + " and IdTipoReg = 3").page(params[:page]).per(10)
              @count_busquedas = @resultado_alertas.count()

              @count_graph_states = Tbitacora.where(query + " and IdTipoReg = 3").group(:IdEstado).count()

              arreglo_graph_states = Array.new

              @count_graph_states.to_a.each do |res|
                @id_state = res[0]
                @cant = res[1]

                @find_estado = Testadosxalerta.find(@id_state)

                valor = {:tipo => "", :cantidad => ""}
                valor[:tipo] = @find_estado.Descripcion
                valor[:cantidad] = @cant

                arreglo_graph_states.push(valor)
              end

              @arreglo_graph_states = arreglo_graph_states

            elsif params[:search_for_users].present?
              @search_for_users = params[:search_for_users]
              puts 'busco para users'
              @par_users = params[:valor_usuarios]
              @usuarios_busqueda = @par_users.split(',')
              @estado_us = params[:state_users]
              @producto = params[:product_users]
              @filtro = params[:filter_users]
              @grupo = params[:groups_users]
              @perfil = params[:profile_users]
              @fecha_inicio = params[:inicio_users]
              @fecha_fin = params[:fin_users]
              @campos_lay = params[:field_layout_users]
              @valor_lay = params[:value_lay_users]
              @fraude = params[:fraud_users]
              query = ""

              @count_users = 0
              if @usuarios_busqueda.length == 1
                query = "IdUsuario = '" + @usuarios_busqueda[0] + "'"
              else
                @usuarios_busqueda.each do |st|
                  @count_users += 1

                  if @count_users == 1
                    query = "(IdUsuario = '" + st + "'"
                  else
                    if @count_users < @usuarios_busqueda.length
                      query = query + " or IdUsuario = '" + st + "'"
                    else
                      query = query + " or IdUsuario = '" + st + "')"
                    end
                  end
                end
              end


              # Se ingresó otro parámetro de búsqueda además del estado
              if @estado_us != "" || @producto != "" || @filtro != "" || @grupo != "" || !@perfil.nil? || @fecha_inicio != "" || @fecha_fin != "" || @campos_lay != "" || !@valor_lay.nil? || !@fraude.nil?
                if @estado_us != ""
                  query = query + " and IdEstado = " + @estado_us
                end

                if @producto != ""
                  if @producto == "1"
                    query = query + " and IdProducto = 1"

                    if @filtro != ""
                      query = query + " and IdFiltro = " + @filtro
                    end
                  elsif @producto == "2"
                    query = query +" and IdProducto = 2"

                    if @grupo != ""
                      query = query + " and IdGrupo = " + @grupo

                      if !@perfil.nil? && @perfil != ""
                        query = query + " and IdPerfil = " + @perfil
                      end
                    end
                  end
                end

                if @fecha_inicio != ""
                  @fecha_inicio = @fecha_inicio.to_date.strftime('%Y-%m-%d')
                  if @fecha_fin != ""
                    @fecha_fin = @fecha_fin.to_date.strftime('%Y-%m-%d')
                    query = query + " and Fecha between '" + @fecha_inicio + "' and '" + @fecha_fin + "' and Hora between '00:00:00.00000' and '23:59:59.99999'"
                  end
                end

                if @campos_lay != ""
                  if @valor_lay != "" && !@valor_lay.nil?
                    @campo = Tcamposlay.where(:IdCampo => @campos_lay).first
                    query = query + " and Referencia like '%" + @campo.Nombre + " = " + @valor_lay.to_s + "%'"
                  end
                end

                if !@fraude.nil?
                  if @fraude == "1"
                    query = query + " and FalsoPositivo = 1"
                  elsif @fraude == "0"
                    query = query + " and FalsoPositivo = 0"
                  end
                end
              end

              # @resultado_alertas = Tbitacora.where(:IdTipoReg => 3).page(params[:page]).per(10)
              @resultado_alertas = Tbitacora.where(query + " and IdTipoReg = 3").page(params[:page]).per(10)
              @count_busquedas = @resultado_alertas.count()

              @count_graph_users = Tbitacora.where(query + " and IdTipoReg = 3").group(:IdUsuario).count()

              arreglo_graph_users = Array.new

              @count_graph_users.to_a.each do |res|
                @id_us = res[0]
                @cant = res[1]

                @find_usuario = User.find(@id_us)

                valor = {:usr_name => "", :cantidad => ""}
                valor[:usr_name] = @find_usuario.user_name
                valor[:cantidad] = @cant

                arreglo_graph_users.push(valor)
              end

              @arreglo_graph_users = arreglo_graph_users
            end
            # FIN BUSQUEDAS

            # Generación de XLS
            if params[:xls_g]
              Thread.new do
                puts red('Comienza hilo-- NIVELES DE ALERTAMIENTO')
                @user_id = params[:user_id]
                @idioma = params[:idioma]

                if params[:search_for_states].present?
                  puts green('Exportación de filtro estados')

                  @filtro_generador = params[:search_for_states]

                  @par_estados = params[:valor_estados]
                  @estados_busqueda = @par_estados.split(',')
                  @usuarios_states = params[:users_states]
                  @producto = params[:product_states]
                  @filtro = params[:filter_states]
                  @grupo = params[:groups_states]
                  @perfil = params[:profile_states]
                  @fecha_inicio = params[:inicio_states]
                  @fecha_fin = params[:fin_states]
                  @campos_lay = params[:field_layout_states]
                  @valor_lay = params[:value_lay_states]
                  @fraude = params[:fraud_states]
                  query = ""


                  @count_states = 0
                  if @estados_busqueda.length == 1
                    query = "IdEstado = " + @estados_busqueda[0]
                  else
                    @estados_busqueda.each do |st|
                      @count_states += 1

                      if @count_states == 1
                        query = "(IdEstado = " + st
                      else
                        if @count_states < @estados_busqueda.length
                          query = query + " or IdEstado = " + st
                        else
                          query = query + " or IdEstado = " + st + ")"
                        end
                      end
                    end
                  end


                  # Se ingresó otro parámetro de búsqueda además del estado
                  if @usuarios_states != "" || @producto != "" || @filtro != "" || @grupo != "" || !@perfil.nil? || @fecha_inicio != "" || @fecha_fin != "" || @campos_lay != "" || !@valor_lay.nil? || !@fraude.nil?
                    if @usuarios_states != ""
                      query = query + " and IdUsuario = " + @usuarios_states.to_s
                    end

                    if @producto != ""
                      if @producto == "1"
                        query = query + " and IdProducto = 1"

                        if @filtro != ""
                          query = query + " and IdFiltro = " + @filtro
                        end
                      elsif @producto == "2"
                        query = query +" and IdProducto = 2"

                        if @grupo != ""
                          query = query + " and IdGrupo = " + @grupo

                          if !@perfil.nil? && @perfil != ""
                            query = query + " and IdPerfil = " + @perfil
                          end
                        end
                      end
                    end

                    if @fecha_inicio != ""
                      @fecha_inicio = @fecha_inicio.to_date.strftime('%Y-%m-%d')
                      if @fecha_fin != ""
                        @fecha_fin = @fecha_fin.to_date.strftime('%Y-%m-%d')
                        query = query + " and Fecha between '" + @fecha_inicio + "' and '" + @fecha_fin + "' and Hora between '00:00:00.00000' and '23:59:59.99999'"
                      end
                    end

                    if @campos_lay != ""
                      if @valor_lay != "" && !@valor_lay.nil?
                        @campo = Tcamposlay.where(:IdCampo => @campos_lay).first
                        query = query + " and Referencia like '%" + @campo.Nombre + " = " + @valor_lay.to_s + "%'"
                      end
                    end

                    if !@fraude.nil?
                      if @fraude == "1"
                        query = query + " and FalsoPositivo = 1"
                      elsif @fraude == "0"
                        query = query + " and FalsoPositivo = 0"
                      end
                    end
                  end

                  # @resultado_alertas = Tbitacora.where(:IdTipoReg => 3).page(params[:page]).per(10)
                  @resultado_alertas = Tbitacora.where(query + " and IdTipoReg = 3")

                  puts cyan('Comienza generación de XLS-- estados')
                  create_xls = CreateXls.new
                  create_xls.setInfoAlertLevelXls(@resultado_alertas, @filtro_generador, @user_id, @idioma)
                  create_xls.alertLevelXLS

                elsif params[:search_for_users].present?
                  puts green('Exportación filtro users')
                  @filtro_generador = params[:search_for_users]

                  @par_users = params[:valor_usuarios]
                  @usuarios_busqueda = @par_users.split(',')
                  @estado_us = params[:state_users]
                  @producto = params[:product_users]
                  @filtro = params[:filter_users]
                  @grupo = params[:groups_users]
                  @perfil = params[:profile_users]
                  @fecha_inicio = params[:inicio_users]
                  @fecha_fin = params[:fin_users]
                  @campos_lay = params[:field_layout_users]
                  @valor_lay = params[:value_lay_users]
                  @fraude = params[:fraud_users]
                  query = ""

                  @count_users = 0
                  if @usuarios_busqueda.length == 1
                    query = "IdUsuario = " + @usuarios_busqueda[0]
                  else
                    @usuarios_busqueda.each do |st|
                      @count_users += 1

                      if @count_users == 1
                        query = "(IdUsuario = " + st
                      else
                        if @count_users < @usuarios_busqueda.length
                          query = query + " or IdUsuario = " + st
                        else
                          query = query + " or IdUsuario = " + st + ")"
                        end
                      end
                    end
                  end


                  # Se ingresó otro parámetro de búsqueda además del estado
                  if @estado_us != "" || @producto != "" || @filtro != "" || @grupo != "" || !@perfil.nil? || @fecha_inicio != "" || @fecha_fin != "" || @campos_lay != "" || !@valor_lay.nil? || !@fraude.nil?
                    if @estado_us != ""
                      query = query + " and IdEstado = " + @estado_us
                    end

                    if @producto != ""
                      if @producto == "1"
                        query = query + " and IdProducto = 1"

                        if @filtro != ""
                          query = query + " and IdFiltro = " + @filtro
                        end
                      elsif @producto == "2"
                        query = query +" and IdProducto = 2"

                        if @grupo != ""
                          query = query + " and IdGrupo = " + @grupo

                          if !@perfil.nil? && @perfil != ""
                            query = query + " and IdPerfil = " + @perfil
                          end
                        end
                      end
                    end

                    if @fecha_inicio != ""
                      @fecha_inicio = @fecha_inicio.to_date.strftime('%Y-%m-%d')
                      if @fecha_fin != ""
                        @fecha_fin = @fecha_fin.to_date.strftime('%Y-%m-%d')
                        query = query + " and Fecha between '" + @fecha_inicio + "' and '" + @fecha_fin + "' and Hora between '00:00:00.00000' and '23:59:59.99999'"
                      end
                    end

                    if @campos_lay != ""
                      if @valor_lay != "" && !@valor_lay.nil?
                        @campo = Tcamposlay.where(:IdCampo => @campos_lay).first
                        query = query + " and Referencia like '%" + @campo.Nombre + " = " + @valor_lay.to_s + "%'"
                      end
                    end

                    if !@fraude.nil?
                      if @fraude == "1"
                        query = query + " and FalsoPositivo = 1"
                      elsif @fraude == "0"
                        query = query + " and FalsoPositivo = 0"
                      end
                    end
                  end

                  # @resultado_alertas = Tbitacora.where(:IdTipoReg => 3).page(params[:page]).per(10)
                  @resultado_alertas = Tbitacora.where(query + " and IdTipoReg = 3")

                  puts cyan('Comienza generación de XLS-- users')
                  create_xls = CreateXls.new
                  create_xls.setInfoAlertLevelXls(@resultado_alertas, @filtro_generador, @user_id, @idioma)
                  create_xls.alertLevelXLS
                end


              end
            end
          else
            @sin_permisos = true
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_enabled')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end

    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end

  end

  def red(mytext)
    ; "\e[31m#{mytext}\e[0m";
  end

  def green(mytext)
    ; "\e[32m#{mytext}\e[0m";
  end

  def cyan(mytext)
    ; "\e[36m#{mytext}\e[0m"
  end
end
