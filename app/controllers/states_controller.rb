class StatesController < ApplicationController
  before_action :set_state, only: [:show, :edit, :update, :destroy]

  # GET /states
  # GET /states.json
  def index
    @id_usuario = current_user.id
    @id_aplicacion = 25
    @idTipoReg = 11 #Index
    @fecha = Time.now.strftime("%F")
    @hora = Time.now.strftime("%k:%M:%S.00000")
    @men = "Estados por Alerta Index"


    @tbit = Tbitacora.new
    @tbit.IdUsuario = @id_usuario
    @tbit.IdAplicacion = @id_aplicacion
    @tbit.IdTipoReg = @idTipoReg #Index
    @tbit.Fecha = @fecha
    @tbit.Hora  = @hora
    @tbit.Mensaje = @men
    @tbit.save
    # ------ Va a mostrar todos los estados en index primero

    @states = Testadosxalerta.all.page(params[:page]).per(10)


    #------ Si los parámetros están presentes entonces van a mostrar la validacion
    #---------- Modal ----------
    if params[:search_for_states].present?
      @search_for_states = params[:search_for_states]
      puts 'busco para estados'
      @par_estados = params[:valor_estados]
      @estados_busqueda = @par_estados.split(',')
      @usuarios_states = params[:users_states]
      @fraude = params[:fraud_states]
      query = ""
      filtro = ""


      @count_states = 0
      if @estados_busqueda.length == 1
        query = "IdEstado = " + @estados_busqueda[0]
        est = Testadosxalerta.find(@estados_busqueda[0])
        filtro = "#{t('views.states_index.name')}: " + est.Descripcion
      else
        @estados_busqueda.each do |st|
          @count_states += 1

          if @count_states == 1
            query = "(IdEstado = " + st
            uno = Testadosxalerta.find(st)
            filtro = "#{t('views.states_index.name')}: #{uno.Descripcion}"
          else
            if @count_states < @estados_busqueda.length
              query = query + " or IdEstado = " + st
              dos = Testadosxalerta.find(st)
              filtro += ", #{t('views.states_index.st_or')} #{t('views.states_index.name')}: #{dos.Descripcion},"
            else
              query = query + " or IdEstado = " + st + ")"
              tres = Testadosxalerta.find(st)
              filtro += ", #{t('views.states_index.st_or')} #{t('views.states_index.name')}: #{tres.Descripcion}"
            end
          end
        end
      end


      if @usuarios_states != "" || !@fraude.nil?
        if @usuarios_states != ""
          m = Testadosxalerta.find_by_IdEstado(@usuarios_states.to_i)
          @mens = m.mensajeDefault
          if !@mens.nil?
            query = query + " and mensajeDefault = '" + @mens.to_s + "'"
            filtro += ", #{t('views.states_index.message_default')}: '" + @mens.to_s + "'"
          else
            query = query + " and mensajeDefault is null"
            filtro += ", #{t('views.states_index.message_default')}: N/A"
          end
        end


        if !@fraude.nil?
          if @fraude == "1"
            query = query + " and falso_positivo = 1"
            filtro += ", #{t('views.states_index.fraud')}: Si Fraude"
          elsif @fraude == "0"
            query = query + " and falso_positivo = 0"
            filtro += ", #{t('views.states_index.fraud')}: No es Fraude"
          elsif @fraude == "2"
            query = query + " and falso_positivo = 2"
            filtro += ", #{t('views.states_index.fraud')}: Por Definir"
          end
        end
      end


      @resultado_alertas = Testadosxalerta.where(query)
      @count_busquedas = @resultado_alertas.count()

      @count_states = Testadosxalerta.where(query).group(:IdEstado).count()
      puts 'Filtro: '+filtro.to_s
      puts 'Consulta: '+query.to_s

      if @count_busquedas > 1
        arreglo_states = Array.new

        @count_states.to_a.each do |res|
          @id_st = res[0]
          @cant = res[1]

          @find_state = Testadosxalerta.find(@id_st)

          valor = {:name => "", :fraude => "", :mensaje => "", :est => "", :cantidad => ""}
          valor[:name] = @find_state.Descripcion
          valor[:fraude] = @find_state.falso_positivo
          valor[:mensaje] = @find_state.mensajeDefault
          valor[:est] = @find_state.IdEstado
          valor[:cantidad] = @cant


          arreglo_states.push(valor)
        end
        @arreglo_states = arreglo_states
        if @arreglo_states.any?
          @filtro = filtro
          @estados = Kaminari.paginate_array(@arreglo_states).page(params[:page]).per(10)
        else
          @states = Testadosxalerta.all.page(params[:page]).per(10)
          @error = true
        end
      else
        #@arreglo_states = Testadosxalerta.where(query)
        @states = Testadosxalerta.where(query).page(params[:page]).per(10)
        if @states.present?
          @states = Testadosxalerta.where(query).page(params[:page]).per(10)
          @filtro = filtro
        else
          @states = Testadosxalerta.all.page(params[:page]).per(10)
          @error = true
        end


      end

    end



  end

  # GET /states/1
  # GET /states/1.json
  def show
    @id_usuario = current_user.id
    @id_aplicacion = 25
    @idTipoReg = 13 #Show
    @fecha = Time.now.strftime("%F")
    @hora = Time.now.strftime("%k:%M:%S.00000")
    @men = "Estados por Alerta Show"

    @tbit = Tbitacora.new
    @tbit.IdUsuario = @id_usuario
    @tbit.IdAplicacion = @id_aplicacion
    @tbit.IdTipoReg = @idTipoReg #Index
    @tbit.Fecha = @fecha
    @tbit.Hora  = @hora
    @tbit.Mensaje = @men
    @tbit.save
    @state = Testadosxalerta.find_by_IdEstado(params[:id])
  end

  # GET /states/new
  def new
    #@state = Testadosxalerta.find_by_IdEstado(params[:id])
    @state = Testadosxalerta.new
  end

  # GET /states/1/edit
  def edit
    @state = Testadosxalerta.find_by_IdEstado(params[:id])
    #@state = Testadosxalerta.where(:IdEstado => params[:id])

  end

  # POST /states
  # POST /states.json
  def create
    @id_usuario = current_user.id
    @id_aplicacion = 25
    @idTipoReg = 9 #Create
    @fecha = Time.now.strftime("%F")
    @hora = Time.now.strftime("%k:%M:%S.00000")
    @men = "Estados por Alerta New"
    # alertas = cuando se entre a editar alerta se va a guardar el id de tbitacora(alerta que se está atendiendo) y el id de producto, transaccional o perfiles <== aun no


    @tbit = Tbitacora.new
    @tbit.IdUsuario = @id_usuario
    @tbit.IdAplicacion = @id_aplicacion
    @tbit.IdTipoReg = @idTipoReg #Edit
    @tbit.Fecha = @fecha
    @tbit.Hora  = @hora
    @tbit.Mensaje = @men
    @tbit.save

    @state = Testadosxalerta.new(state_params)
    #@state = Testadosxalerta.find_by_IdEstado(params[:id])
    respond_to do |format|
      #@state = Testadosxalerta.find_by_IdEstado(params[:id])
      if params[:yes].present?
        @state.falso_positivo = '1'
      elsif params[:no].present?
        @state.falso_positivo = '0'
      elsif params[:porDef].present?
        @state.falso_positivo = '2'
      end
      if @state.save
        format.html {redirect_to @state, notice: 'State was successfully created.'}
        format.json {render :show, status: :created, location: @state}
      else
        format.html {render :new}
        format.json {render json: @state.errors, status: :unprocessable_entity}
      end
    end
  end

  # PATCH/PUT /states/1
  # PATCH/PUT /states/1.json
  def update
    @id_usuario = current_user.id
    @id_aplicacion = 25
    @idTipoReg = 10 #Edit
    @fecha = Time.now.strftime("%F")
    @hora = Time.now.strftime("%k:%M:%S.00000")
    @men = "Estados por Alerta Edit"
    # alertas = cuando se entre a editar alerta se va a guardar el id de tbitacora(alerta que se está atendiendo) y el id de producto, transaccional o perfiles <== aun no


    @tbit = Tbitacora.new
    @tbit.IdUsuario = @id_usuario
    @tbit.IdAplicacion = @id_aplicacion
    @tbit.IdTipoReg = @idTipoReg #Edit
    @tbit.Fecha = @fecha
    @tbit.Hora  = @hora
    @tbit.Mensaje = @men
    @tbit.save

    @state = Testadosxalerta.find_by_IdEstado(params[:id])
    respond_to do |format|
      @state = Testadosxalerta.find_by_IdEstado(params[:id])
      if params[:yes].present?
        @state.falso_positivo = '1'
      elsif params[:no].present?
        @state.falso_positivo = '0'
      elsif params[:porDef].present?
        @state.falso_positivo = '2'
      end
      if @state.update(state_params)
        format.html {redirect_to @state, notice: 'State was successfully updated.'}
        format.json {render :show, status: :ok, location: @state}
      else
        format.html {render :edit}
        format.json {render json: @state.errors, status: :unprocessable_entity}
      end
    end
  end

  # DELETE /states/1
  # DELETE /states/1.json
  def destroy
    @id_usuario = current_user.id
    @id_aplicacion = 25
    @idTipoReg = 12 #Index
    @fecha = Time.now.strftime("%F")
    @hora = Time.now.strftime("%k:%M:%S.00000")
    #@men = "Areas Destroy"


    @tbit = Tbitacora.new
    @tbit.IdUsuario = @id_usuario
    @tbit.IdAplicacion = @id_aplicacion
    @tbit.IdTipoReg = @idTipoReg #Index
    @tbit.Fecha = @fecha
    @tbit.Hora  = @hora

    @state = Testadosxalerta.find_by_IdEstado(params[:id])

    @tbit.Mensaje = @state.to_json
    @tbit.save

    @state.destroy
    respond_to do |format|
      format.html {redirect_to states_url, notice: 'State was successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_state
    @state = Testadosxalerta.where(:IdEstado => params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def state_params
    params.require(:testadosxalerta).permit(:Descripcion, :falso_positivo, :mensajeDefault)
  end
end
