json.extract! table_order, :id, :user_id, :name_column, :order, :table_id, :created_at, :updated_at
json.url table_order_url(table_order, format: :json)
