json.extract! filtering_categorization, :id, :name_categorization, :created_at, :updated_at
json.url filtering_categorization_url(filtering_categorization, format: :json)
