class XlsConGenMailer < ApplicationMailer


  default from: "krodriguez@kssoluciones.com.mx"

  def send_con_gen_xls_es(fecha_comienzo, usuario, file, fecha_termino)
    @inicio = fecha_comienzo
    @use = usuario.name + ' ' + usuario.last_name
    @file = file
    @path_file = 'public/generatedXLS/con_gen/'+ @file
    @fin = fecha_termino

    attachments[@file] = File.read("#{@path_file}")

    #@id_usuario = current_user.id
    @id_usuario = usuario.id
    @id_aplicacion = 25
    @idTipoReg = 15 #Mailer
    @fecha = Time.now.strftime("%F")
    @hora = Time.now.strftime("%k:%M:%S.00000")
    @men = "Correo Exportacion Consulta General"


    @tbit = Tbitacora.new
    @tbit.IdUsuario = @id_usuario
    @tbit.IdAplicacion = @id_aplicacion
    @tbit.IdTipoReg = @idTipoReg #Mailer
    @tbit.Fecha = @fecha
    @tbit.Hora  = @hora
    @tbit.Mensaje = @men
    @tbit.save


    mail to: usuario.email, subject: "Exportación de Resultados de Consulta General a Excel lista: #{@file.to_s}"
  end

  def send_con_gen_xls_en(fecha_comienzo, usuario, file, fecha_termino)
    @inicio = fecha_comienzo
    @use = usuario.name + ' ' + usuario.last_name
    @file = file
    @path_file = 'public/generatedXLS/con_gen/'+ @file
    @fin = fecha_termino

    attachments[@file] = File.read("#{@path_file}")

    #@id_usuario = current_user.id
    @id_usuario = usuario.id
    @id_aplicacion = 25
    @idTipoReg = 15 #Mailer
    @fecha = Time.now.strftime("%F")
    @hora = Time.now.strftime("%k:%M:%S.00000")
    @men = "Export from General Search to Excel mailer "


    @tbit = Tbitacora.new
    @tbit.IdUsuario = @id_usuario
    @tbit.IdAplicacion = @id_aplicacion
    @tbit.IdTipoReg = @idTipoReg #Mailer
    @tbit.Fecha = @fecha
    @tbit.Hora  = @hora
    @tbit.Mensaje = @men
    @tbit.save

    mail to: usuario.email, subject: "Result Export from General Search to Excel is ready: #{@file.to_s}"
  end

end
