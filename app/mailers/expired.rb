class Expired < ApplicationMailer

  #default from: "aflores@kssoluciones.com.mx"
  default from: "krodriguez@kssoluciones.com.mx"
  #default from: "Listas_Blancas@homedepot.com.mx"

  def avisar_activacion(email, user)

    @email = email.to_s
    @usr_name = user.name
    @usr_last_name = user.last_name
    @usr_mail = user.email
    @id_usr = user.id

    #@id_usuario = current_user.id
    @id_usuario = user.id
    @id_aplicacion = 25
    @idTipoReg = 15 #Mailer
    @fecha = Time.now.strftime("%F")
    @hora = Time.now.strftime("%k:%M:%S.00000")
    @men = "Mail Avisar Activacion"


    @tbit = Tbitacora.new
    @tbit.IdUsuario = @id_usuario
    @tbit.IdAplicacion = @id_aplicacion
    @tbit.IdTipoReg = @idTipoReg #Mailer
    @tbit.Fecha = @fecha
    @tbit.Hora  = @hora
    @tbit.Mensaje = @men
    @tbit.save


    mail to: @email, subject: default_i18n_subject(user: user.name)
  end

  def cuenta_activada(user)

    @user_id = user.id
    @user_email = user.email
    @user_name = user.name
    @last_name = user.last_name
    @login_name = user.user_name
    @role = user.profile.name

    #@id_usuario = current_user.id
    @id_usuario = user.id
    @id_aplicacion = 25
    @idTipoReg = 15 #Mailer
    @fecha = Time.now.strftime("%F")
    @hora = Time.now.strftime("%k:%M:%S.00000")
    @men = "Mail Avisar Activacion"


    @tbit = Tbitacora.new
    @tbit.IdUsuario = @id_usuario
    @tbit.IdAplicacion = @id_aplicacion
    @tbit.IdTipoReg = @idTipoReg #Mailer
    @tbit.Fecha = @fecha
    @tbit.Hora  = @hora
    @tbit.Mensaje = @men
    @tbit.save


    @sh_profile = Permission.where("profile_id = ?", user.profile_id )

    mail to: @user_email, subject: default_i18n_subject(user: user.name)

  end
end