class CreateFilteringCategorizations < ActiveRecord::Migration[5.0]
  def change
    create_table :filtering_categorizations do |t|
      t.string :name_categorization

      t.timestamps
    end
  end
end
