class CreateGestorSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :gestor_settings do |t|
      t.string :blue_day
      t.string :blue_nigth
      t.string :yellow_day
      t.string :yellow_nigth
      t.string :red_day
      t.string :red_nigth

      t.timestamps
    end
  end
end
