class AddFiltringCategorizationIdToTxFilters < ActiveRecord::Migration[5.0]
  def change
    add_column :tx_filters, :filtering_categorization_id, :integer
  end
end
