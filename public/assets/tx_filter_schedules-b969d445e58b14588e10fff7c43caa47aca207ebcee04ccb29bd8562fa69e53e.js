console.log('Entre horarios filtros');

$(document).ready(function(){
    let index = $('#index').val();

    if( index != "index" ){

        let edit = $('#edit').val(),
            idFiltro = $('#idFiltro_Sch').val();

        //Time Picker
        $('.clockpicker').clockpicker();

        //LLenar tabala con los campos del layout
        for (let i = 0; i < gon.filtros.length; i++){
            let idFiltroGon = gon.filtros[i].Id,
                idSession = gon.filtros[i].Session_id;

            if ( idFiltro == idFiltroGon){
                // alert('Id de filtros igual');

                for (let i = 0; i < gon.sesiones.length; i++){
                    let sesionId = gon.sesiones[i].Id
                    let layId = gon.sesiones[i].Lay_id
                    let formatId = gon.sesiones[i].Format_id

                    if ( idSession == sesionId ){
                        // alert('Sessiones iguales');


                        for ( let c = 0; c < gon.tCamposFormatos.length; c++ ){

                            if ( (gon.tCamposFormatos[c].IdLay == layId ) && ( gon.tCamposFormatos[c].IdFormato == formatId) ){

                                for( let d = 0; d < gon.tCamposLays.length; d++ ){

                                    if ( ( gon.tCamposLays[d].IdLay == gon.tCamposFormatos[c].IdLay ) && ( gon.tCamposLays[d].IdCampo == gon.tCamposFormatos[c].IdCampo )  ){

                                        //Obtengo el id de campo lo convierto a string y saco su longitud.
                                        let idCampo = `${gon.tCamposLays[d].IdCampo}`,
                                            idCampoStr = String(idCampo).length;

                                        //Agrega 0, 00 al id dependiendo del decimal del id de campo
                                        if ( idCampoStr == '1' ){

                                            // console.log(`Agrego 2 ceros a: 00${gon.tCamposLays[d].IdCampo}`);
                                            numIdCampo = `00${idCampo}`;

                                        } else if ( idCampoStr == '2' ){

                                            // console.log(`Agrego 1 ceros a: 0${gon.tCamposLays[d].IdCampo}`);
                                            numIdCampo = `0${idCampo}`;

                                        } else {
                                            // console.log(`El Id es de 3 digitos: ${gon.tCamposLays[d].IdCampo}`);
                                            numIdCampo = `${idCampo}`;
                                        }


                                        if ( gon.tCamposLays[d].Alias != ""){

                                            //Tr para la seccion de la moda de los campos
                                            trCampoModal = `<tr id="${gon.tCamposLays[d].Longitud}">
                                                               <td>${numIdCampo}_${gon.tCamposLays[d].Alias}</td> 
                                                            </tr>`;

                                        } else{

                                            //Tr para la seccion de la moda de los campos
                                            trCampoModal = `<tr id="${gon.tCamposLays[d].Longitud}">
                                                               <td>${numIdCampo}_${gon.tCamposLays[d].Nombre}</td> 
                                                            </tr>`;
                                        }

                                        $('#campos_modal_table').append(trCampoModal);

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if ( edit == "edit" ) {
            // alert('Estoy en alert')

            // Varaiables para obtener los valores de los inputs
            let idFiltro = $('#filtro').val(),
                max_tran = $('#max_tran').val(),
                min_tran = $('#min_tran').val(),
                umInterval = $('#umInterval').val(),
                umIntermediate = $('#umIntermediate').val(),
                umTimeout = $('#umTimeout').val();

            //Asigno los valores a los selects
            $('#filters').val(`${idFiltro}`);
            $('#select_Interval_time').val(`${umInterval}`);
            $('#select_intermediate_time').val(`${umIntermediate}`);
            $('#select_timeout_time').val(`${umTimeout}`);


            if ( max_tran != '0'){

                $('#limit_tran').val(`${max_tran}`);
                $('#select_limit').val('1');

            } else if ( min_tran != '0' ){

                $('#limit_tran').val(`${min_tran}`);
                $('#select_limit').val('2');

            }
        }
    }
});

let longitud;
$('#campos_modal_table').on('dblclick', 'tr', function(){
    let $_this = this,
        txtCampo = $(this).children('td').eq(0).html();

    longitud = $_this.id;

    // alert('Doble click');
    // console.log($_this);
    // console.log(txtCampo);

    //Pimer Campo de Longitd
    $('#long_campo, #longInputConf').val(`${longitud}`); //campo oculto de la longitud
    $('#posInputConf').val('1');
    //Labels con longitud y texto de campo selecionado
    $('#longCampoSelect').html(`${longitud}`);
    $('#campoSelect').html(`${txtCampo}`);
});

//Oculta inputs en la modal dependiendo del radio button que eligan
function fadeCampos(selector){
    selector.fadeIn().siblings().fadeOut();
    // selector.siblings().find('input').val('');
}

//Muestra los inputs dependiendo del radio elegido y ejecuta la función de arriba
$('.content-radios').on('click', 'input:radio', function(){
    let tipo = this.id
    // alert(`Click en el radio: ${tipo}`);

    if ( tipo == 'primeros' ){

        fadeCampos($('div.primeros'))

    } else if ( tipo == 'ultimos' ) {

        fadeCampos($('div.ultimos'))

    } else {
        fadeCampos($('div.rangos'))
    }

});

// funcion que agrega ceros al id del campo
function agregaCeros( valor , longitudValor ){
    // alert('Entre funcion')
    console.log(`Soy valor: ${valor}`)
    console.log(`Soy longitud del campo: ${longitudValor}`)

    let newVal;

    if( longitudValor == "1" ){
        // alert('Agregando 2 ceros')
        newVal = `00${valor}`;

    } else if (longitudValor == '2'){

        // alert('Agregando 1')
        newVal = `0${valor}`;

    } else if ( longitudValor == '3') {
        // alert('Sin ceros')
        newVal = `${valor}`;
    }

    return newVal;
}


//Agregar comentario al text area
$('#btnAddMsn').on('click', function(){
    // alert('Entre add')
    let radioCheck = $('.content-radios').find('input:checked').attr('id'),
        nameCampo = $('#campoSelect').html(),
        contentTextA = $('#mensaje_alerta').val(),
        nomenclature,
        Ltrue = false;

    if ( radioCheck == 'primeros' ){
        // alert('Entre Primeros')
        let primInput = $('#primerosInp').val(),
            tamaPrim = primInput.length,
            result = agregaCeros( primInput, tamaPrim );

        nomenclature = `{${nameCampo}-P(${result})}`;

    } else if ( radioCheck == 'ultimos' ) {
        // alert('Entre ultimos')

        let ultimInput = $('#ultimosInp').val(),
            tamaUltim = ultimInput.length,
            result = agregaCeros( ultimInput, tamaUltim );

        nomenclature = `{${nameCampo}-U(${result})}`;

    } else {

        let posInput = $('#posInputConf').val(),
            lengInput = $('#longInputConf').val(),
            tamaPos = posInput.length,
            tamaLeng = lengInput.length;


        let rUno = agregaCeros( posInput, tamaPos ),
            rDos = agregaCeros(lengInput, tamaLeng);
        // console.log(rUno)
        // console.log(rDos)

        nomenclature = `{${nameCampo}-D(${rUno},${rDos})}`;

    }

    if( $('#dividir').prop('checked') ){
        let str1 = nomenclature.split('}');

        nomenclature = `${str1[0]}/100}`;
    }

    // console.log(nomenclature)

    $('#mensaje_alerta').val(`${contentTextA}${nomenclature}`);
    $('#longCampoSelect').html(``);
    $('#campoSelect').html(``);
    $('.content_config').find('input').val('');
    $('#dividir').attr('checked', false);
});

//Evalua el cambios de los checkbox elejido
$('label.btn').on('click', function(){
    let input = $(this).children('input'),
        valInput = input.val();

    if( valInput == "0"){
        input.val('1')
    } else {
        input.val('0')
    }

});

//Resetea los todos los valores en la modal
function resetModal(){
    $('#mensaje_alerta').val('');
    $('.inputsCheckboxMsn').find('input').attr('checked', false)
    $('label.btn').removeClass('active');
    $('#fechaMsn, #desMsn, #escalaMsn, #limitesMsn, #referenciMsn').val('0');
}

//Boton guardar mensaje de la modal
let contAlert = 0,
    mensaje_alert = [];
$('#saveMsn').on('click', function(){
    let mensajeAlerta = $('#mensaje_alerta').val(),
        fechaMsn = $('#fechaMsn').val(),
        desMsn = $('#desMsn').val(),
        escalaMsn = $('#escalaMsn').val(),
        limitesMsn = $('#limitesMsn').val(),
        referenciMsn = $('#referenciMsn').val();

    if( mensajeAlerta != ''){
        contAlert++;
        let trMsn = `<tr>
                        <td class="text-center mensajeInfo">${contAlert} <input type="hidden" name="msn[dataMsn${contAlert}][Num_Alarma]" value="${contAlert}"></td>
                        <td class="mensajeInfo">${mensajeAlerta} <input type="hidden" name="msn[dataMsn${contAlert}][message]" value="${mensajeAlerta}"></td>
                        <!--<td class="text-center"><span class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></span></td>-->
                        <!--<td class="text-center"><span class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></span></td>-->
                        <td class="mensajeInfo" style="display: none">${fechaMsn} <input type="hidden" name="msn[dataMsn${contAlert}][bit_date]" value="${fechaMsn}"></td>
                        <td class="mensajeInfo" style="display: none">${desMsn} <input type="hidden" name="msn[dataMsn${contAlert}][bit_description]" value="${desMsn}"></td>
                        <td class="mensajeInfo" style="display: none">${escalaMsn} <input type="hidden" name="msn[dataMsn${contAlert}][bit_escala]" value="${escalaMsn}"></td>
                        <td class="mensajeInfo" style="display: none">${limitesMsn} <input type="hidden" name="msn[dataMsn${contAlert}][bit_limit]" value="${limitesMsn}"></td>
                        <td class="mensajeInfo" style="display: none">${referenciMsn} <input type="hidden" name="msn[dataMsn${contAlert}][bit_reference]" value="${referenciMsn}"></td>
                    </tr>`;

        $('.mensaje-de-alerta').append(trMsn);

        // mensaje_alert.push(contAlert,mensajeAlerta,fechaMsn,desMsn,escalaMsn,limitesMsn,referenciMsn);
        //
        // //valores del mensaje
        // $('#mensaje_alerta_arr').val(`${mensaje_alert}`);

        //resetear valores de la modal
        resetModal();

        if( contAlert == 2 ){

            $('.optional').css('display','block');
            $('#select_intermediate_time').val('0');
            $('#select_intermediate_time').val('0');
            $('#select_timeout_time').val('0');
            $('#intermediate').val('').prop('required', true);
            $('#timeOut').val('').prop('required', true);

        }
    }
});

//Boton cerrar de la modal
$('#btncloseModal').on('click', function(){
    resetModal();
});
