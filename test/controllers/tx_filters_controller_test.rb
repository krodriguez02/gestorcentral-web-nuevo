require 'test_helper'

class TxFiltersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tx_filters = tx_filters(:one)
  end

  test "should get index" do
    get tx_filters_url
    assert_response :success
  end

  test "should get new" do
    get new_tx_filters_url
    assert_response :success
  end

  test "should create tx_filters" do
    assert_difference('TxFilter.count') do
      post tx_filters_url, params: { tx_filters: {  } }
    end

    assert_redirected_to tx_filters_url(TxFilter.last)
  end

  test "should show tx_filters" do
    get tx_filters_url(@tx_filters)
    assert_response :success
  end

  test "should get edit" do
    get edit_tx_filters_url(@tx_filters)
    assert_response :success
  end

  test "should update tx_filters" do
    patch tx_filters_url(@tx_filters), params: { tx_filters: {  } }
    assert_redirected_to tx_filters_url(@tx_filters)
  end

  test "should destroy tx_filters" do
    assert_difference('TxFilter.count', -1) do
      delete tx_filters_url(@tx_filters)
    end

    assert_redirected_to tx_filters_url
  end
end
