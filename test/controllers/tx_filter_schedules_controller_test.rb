require 'test_helper'

class TxFilterSchedulesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tx_filter_schedule = tx_filter_schedules(:one)
  end

  test "should get index" do
    get tx_filter_schedules_url
    assert_response :success
  end

  test "should get new" do
    get new_tx_filter_schedule_url
    assert_response :success
  end

  test "should create tx_filter_schedule" do
    assert_difference('TxFilterSchedule.count') do
      post tx_filter_schedules_url, params: { tx_filter_schedule: { Accumulated_Amount: @tx_filter_schedule.Accumulated_Amount, Hr_End: @tx_filter_schedule.Hr_End, Hr_Start: @tx_filter_schedule.Hr_Start, Intermediate: @tx_filter_schedule.Intermediate, Interval: @tx_filter_schedule.Interval, Max_Tra: @tx_filter_schedule.Max_Tra, Min_Tran: @tx_filter_schedule.Min_Tran, OMTimeout: @tx_filter_schedule.OMTimeout, TimeOut: @tx_filter_schedule.TimeOut, Type_Evaluation_Amount: @tx_filter_schedule.Type_Evaluation_Amount, UMIntermediate: @tx_filter_schedule.UMIntermediate, UMInterval: @tx_filter_schedule.UMInterval, filter_id: @tx_filter_schedule.filter_id } }
    end

    assert_redirected_to tx_filter_schedule_url(TxFilterSchedule.last)
  end

  test "should show tx_filter_schedule" do
    get tx_filter_schedule_url(@tx_filter_schedule)
    assert_response :success
  end

  test "should get edit" do
    get edit_tx_filter_schedule_url(@tx_filter_schedule)
    assert_response :success
  end

  test "should update tx_filter_schedule" do
    patch tx_filter_schedule_url(@tx_filter_schedule), params: { tx_filter_schedule: { Accumulated_Amount: @tx_filter_schedule.Accumulated_Amount, Hr_End: @tx_filter_schedule.Hr_End, Hr_Start: @tx_filter_schedule.Hr_Start, Intermediate: @tx_filter_schedule.Intermediate, Interval: @tx_filter_schedule.Interval, Max_Tra: @tx_filter_schedule.Max_Tra, Min_Tran: @tx_filter_schedule.Min_Tran, OMTimeout: @tx_filter_schedule.OMTimeout, TimeOut: @tx_filter_schedule.TimeOut, Type_Evaluation_Amount: @tx_filter_schedule.Type_Evaluation_Amount, UMIntermediate: @tx_filter_schedule.UMIntermediate, UMInterval: @tx_filter_schedule.UMInterval, filter_id: @tx_filter_schedule.filter_id } }
    assert_redirected_to tx_filter_schedule_url(@tx_filter_schedule)
  end

  test "should destroy tx_filter_schedule" do
    assert_difference('TxFilterSchedule.count', -1) do
      delete tx_filter_schedule_url(@tx_filter_schedule)
    end

    assert_redirected_to tx_filter_schedules_url
  end
end
